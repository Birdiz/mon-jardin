<?php

namespace App\Enum;

enum CultureTypeEnum: string
{
    case DIRECT = 'direct';

    case REPLANT = 'repiquage';

    case BOTH = 'direct ou repiquage';
}
