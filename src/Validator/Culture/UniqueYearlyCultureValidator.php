<?php

namespace App\Validator\Culture;

use App\Entity\Culture;
use App\Repository\CultureRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class UniqueYearlyCultureValidator extends ConstraintValidator
{
    public function __construct(private CultureRepository $cultureRepository)
    {
    }

    public function validate(mixed $value, Constraint $constraint): void
    {
        if (!$constraint instanceof UniqueYearlyCulture) {
            throw new UnexpectedTypeException($constraint, UniqueYearlyCulture::class);
        }

        if (!$value instanceof Culture) {
            return;
        }

        $culture = $this->cultureRepository->findOneExisting($value);

        if (!$culture instanceof Culture) {
            return;
        }

        // Happens after edit.
        if ($value->getId() === $culture->getId()) {
            return;
        }

        $valueDate = $value->getSowedAt() ?? $value->getReplantedAt();
        $cultureDate = $culture->getSowedAt() ?? $culture->getReplantedAt();

        if ($cultureDate->format('Y') !== $valueDate->format('Y')) {
            return;
        }

        $this
            ->context
            ->buildViolation($constraint->message)
            ->addViolation()
        ;
    }
}
