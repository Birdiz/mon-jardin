<?php

namespace App\Validator\Culture;

use App\Entity\Culture;
use App\Repository\CultureRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class RequiredDatesValidator extends ConstraintValidator
{
    public function validate(mixed $value, Constraint $constraint): void
    {
        if (!$constraint instanceof RequiredDates) {
            throw new UnexpectedTypeException($constraint, RequiredDates::class);
        }

        if (!$value instanceof Culture) {
            return;
        }

        if ($value->getSowedAt() instanceof \DateTimeImmutable || $value->getReplantedAt() instanceof \DateTimeImmutable) {
            return;
        }

        $this
            ->context
            ->buildViolation($constraint->message)
            ->addViolation()
        ;
    }
}
