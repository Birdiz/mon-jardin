<?php

namespace App\Validator\Culture;

use App\Entity\Culture;
use App\Repository\CultureRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class SowingReplantingDatesValidator extends ConstraintValidator
{
    public function validate(mixed $value, Constraint $constraint): void
    {
        if (!$constraint instanceof SowingReplantingDates) {
            throw new UnexpectedTypeException($constraint, SowingReplantingDates::class);
        }

        if (!$value instanceof Culture) {
            return;
        }

        if ($value->getSowedAt() < $value->getReplantedAt()) {
            return;
        }

        $this
            ->context
            ->buildViolation($constraint->message)
            ->addViolation()
        ;
    }
}
