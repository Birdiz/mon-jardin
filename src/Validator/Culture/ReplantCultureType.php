<?php

namespace App\Validator\Culture;

use Symfony\Component\Validator\Constraint;

#[\Attribute]
class ReplantCultureType extends Constraint
{
    public string $message = 'Cette culture nécessite un repiquage, donc indiquer date de repiquage.';
    public string $mode = 'strict';

    public function __construct(?string $mode = null, ?string $message = null, ?array $groups = null, $payload = null)
    {
        parent::__construct([], $groups, $payload);

        $this->mode = $mode ?? $this->mode;
        $this->message = $message ?? $this->message;
    }

    public function getTargets(): string|array
    {
        return self::CLASS_CONSTRAINT;
    }
}
