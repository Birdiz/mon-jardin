<?php

namespace App\Validator\Culture;

use App\Entity\Culture;
use App\Enum\CultureTypeEnum;
use App\Repository\CultureRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class ReplantultureTypeValidator extends ConstraintValidator
{
    public function validate(mixed $value, Constraint $constraint): void
    {
        if (!$constraint instanceof RequiredDates) {
            throw new UnexpectedTypeException($constraint, RequiredDates::class);
        }

        if (!$value instanceof Culture) {
            return;
        }

        if ($value->getPlant()->getCultureType() !== CultureTypeEnum::REPLANT || $value->getReplantedAt() instanceof \DateTimeImmutable) {
            return;
        }

        $this
            ->context
            ->buildViolation($constraint->message)
            ->addViolation()
        ;
    }
}
