<?php

namespace App\Validator\Culture;

use Symfony\Component\Validator\Constraint;

#[\Attribute]
class RequiredDates extends Constraint
{
    public string $message = 'Date de semis ou de repiquage requis.';
    public string $mode = 'strict';

    public function __construct(?string $mode = null, ?string $message = null, ?array $groups = null, $payload = null)
    {
        parent::__construct([], $groups, $payload);

        $this->mode = $mode ?? $this->mode;
        $this->message = $message ?? $this->message;
    }

    public function getTargets(): string|array
    {
        return self::CLASS_CONSTRAINT;
    }
}
