<?php

namespace App\Form;

use App\Entity\Plant;
use App\Enum\CultureTypeEnum;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PlantType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(child: 'name', options: [
                'attr' => [
                    'class' => 'input',
                ],
                'label' => 'Nom',
                'label_attr' => [
                    'class' => 'label',
                ],
                'row_attr' => [
                    'class' => 'field',
                ],
            ])
            ->add(child: 'variety', options: [
                'attr' => [
                    'class' => 'input',
                ],
                'label' => 'Variété',
                'label_attr' => [
                    'class' => 'label',
                ],
                'row_attr' => [
                    'class' => 'field',
                ],
            ])
            ->add('cultureType', EnumType::class, [
                'attr' => [
                    'class' => 'input',
                ],
                'class' => CultureTypeEnum::class,
                'choice_label' => function (CultureTypeEnum $cultureTypeEnum): string {
                    return $cultureTypeEnum->value;
                },
                'label' => 'Type de culture',
                'label_attr' => [
                    'class' => 'label',
                ],
                'row_attr' => [
                    'class' => 'field',
                ],
            ])
            ->add(child: 'info', options: [
                'attr' => [
                    'class' => 'textarea',
                ],
                'label_attr' => [
                    'class' => 'label',
                ],
                'required' => false,
                'row_attr' => [
                    'class' => 'field',
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Plant::class,
        ]);
    }
}
