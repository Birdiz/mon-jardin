<?php

namespace App\Form;

use App\Entity\Culture;
use App\Entity\Emplacement;
use App\Entity\Plant;
use App\Repository\EmplacementRepository;
use App\Repository\PlantRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CultureType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(child: 'title', options: [
                'attr' => [
                    'class' => 'input',
                ],
                'label' => 'Titre',
                'label_attr' => [
                    'class' => 'label',
                ],
                'row_attr' => [
                    'class' => 'field',
                ],
            ])
            ->add('emplacement', EntityType::class, [
                'attr' => [
                    'class' => 'input',
                ],
                'class' => Emplacement::class,
                'choice_label' => 'title',
                'label_attr' => [
                    'class' => 'label',
                ],
                'query_builder' => function (EmplacementRepository $er): QueryBuilder {
                    return $er
                        ->createQueryBuilder('e')
                        ->orderBy('e.title', 'ASC')
                        ;
                },
                'row_attr' => [
                    'class' => 'field',
                ],
            ])
            ->add('plant', EntityType::class, [
                'attr' => [
                    'class' => 'input',
                ],
                'class' => Plant::class,
                'choice_label' => function (Plant $plant): string {
                    return $plant->getLabel();
                },
                'label_attr' => [
                    'class' => 'label',
                ],
                'query_builder' => function (PlantRepository $pr): QueryBuilder {
                    return $pr
                        ->createQueryBuilder('p')
                        ->orderBy('p.name', 'ASC')
                        ;
                },
                'row_attr' => [
                    'class' => 'field',
                ],
            ])
            ->add('sowedAt', DateType::class, [
                'attr' => [
                    'class' => 'input',
                ],
                'label' => 'Semé le',
                'label_attr' => [
                    'class' => 'label',
                ],
                'required' => false,
                'row_attr' => [
                    'class' => 'field',
                ],
                'widget' => 'single_text',
            ])
            ->add('replantedAt', DateType::class, [
                'attr' => [
                    'class' => 'input',
                ],
                'label' => 'Repiqué le',
                'label_attr' => [
                    'class' => 'label',
                ],
                'required' => false,
                'row_attr' => [
                    'class' => 'field',
                ],
                'widget' => 'single_text',
            ])
            ->add('harvestedAt', DateType::class, [
                'attr' => [
                    'class' => 'input',
                ],
                'label' => 'Récolté le',
                'label_attr' => [
                    'class' => 'label',
                ],
                'required' => false,
                'row_attr' => [
                    'class' => 'field',
                ],
                'widget' => 'single_text',
            ])
            ->add('quantity', options: [
                'attr' => [
                    'class' => 'input',
                ],
                'label' => 'Quantité (plants, lignes, ...)',
                'label_attr' => [
                    'class' => 'label',
                ],
                'row_attr' => [
                    'class' => 'field',
                ],
            ])
            ->add('isPermanant', CheckboxType::class, [
                'label' => ' Culture permanante?',
                'label_attr' => [
                    'class' => 'checkbox',
                ],
                'required' => false,
                'row_attr' => [
                    'class' => 'field',
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Culture::class,
        ]);
    }
}
