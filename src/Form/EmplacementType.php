<?php

namespace App\Form;

use App\Entity\Emplacement;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmplacementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(child: 'title', options: [
                'attr' => [
                    'class' => 'input',
                ],
                'label' => 'Titre',
                'label_attr' => [
                    'class' => 'label',
                ],
                'row_attr' => [
                    'class' => 'field',
                ],
            ])
            ->add(child: 'description', options: [
                'attr' => [
                    'class' => 'textarea',
                ],
                'label_attr' => [
                    'class' => 'label',
                ],
                'row_attr' => [
                    'class' => 'field',
                ],
            ])
            ->add(child: 'size', options: [
                'attr' => [
                    'class' => 'input',
                ],
                'label' => 'Taille (m2)',
                'label_attr' => [
                    'class' => 'label',
                ],
                'row_attr' => [
                    'class' => 'field',
                ],
            ])
            ->add(
                'orientation',
                ChoiceType::class,
                [
                    'attr' => [
                        'class' => 'input',
                    ],
                    'choices' => ['N/S' => 'N/S', 'E/O' => 'E/O', 'NE/SO' => 'NE/SO', 'NO/SE' => 'NO/SE'],
                    'label_attr' => [
                        'class' => 'label',
                    ],
                    'row_attr' => [
                        'class' => 'field',
                    ],
                ],
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Emplacement::class,
        ]);
    }
}
