<?php

namespace App\Repository;

use App\Entity\Plant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Plant>
 */
class PlantRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Plant::class);
    }

    /**
     * @return Plant[]
     */
    public function search(?string $needle = null): array
    {
        $query = $this->createQueryBuilder('p');

        if (\is_string($needle)) {
            $query
                ->andWhere('LOWER(p.name) LIKE LOWER(:needle) OR LOWER(p.variety) LIKE LOWER(:needle)')
                ->setParameter('needle', '%'.$needle.'%')
            ;
        }

        return $query
            ->orderBy('p.name', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }
}
