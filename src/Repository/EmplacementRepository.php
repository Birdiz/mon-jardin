<?php

namespace App\Repository;

use App\Entity\Emplacement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Emplacement>
 */
class EmplacementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Emplacement::class);
    }

    public function sumEmplacementSizes(): string
    {
        return $this->createQueryBuilder('e')
            ->select('sum(e.size)')
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }
}
