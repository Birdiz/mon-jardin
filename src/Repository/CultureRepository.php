<?php

namespace App\Repository;

use App\Entity\Culture;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Culture>
 */
class CultureRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Culture::class);
    }

    /**
     * @return Culture[] Returns an array of Culture objects
     */
    public function getNextComingCultureToSow(): array
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.sowedAt is not null and c.sowedAt > :now')
            ->setParameter('now', (new \DateTime()))
            ->orderBy('c.sowedAt', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Culture[]
     */
    public function getNextComingCultureToReplant(): array
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.replantedAt is not null and c.replantedAt > :now')
            ->setParameter('now', (new \DateTime()))
            ->orderBy('c.replantedAt', 'ASC')
            ->setMaxResults(5)
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @return Culture[]
     */
    public function getNextComingCultureToHarvest(): array
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.harvestedAt is not null')
            ->orderBy('c.harvestedAt', 'ASC')
            ->setMaxResults(5)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findOneExisting(Culture $value): ?Culture
    {
        return $this->createQueryBuilder('c')
            ->join('c.emplacement', 'e')
            ->join('c.plant', 'p')
            ->where('e.id = :emplacementId')
            ->setParameter('emplacementId', $value->getEmplacement()->getId())
            ->andWhere('p.id = :planteId')
            ->setParameter('planteId', $value->getPlant()->getId())
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    /**
     * @return Culture[]
     */
    public function search(?string $needle = null): array
    {
        $query = $this->createQueryBuilder('c');

        if (\is_string($needle)) {
            $query
                ->andWhere('LOWER(c.title) LIKE LOWER(:needle)')
                ->setParameter('needle', '%'.$needle.'%')
            ;
        }

        return $query
            ->addOrderBy('c.isPermanant', 'ASC')
            ->addOrderBy('CASE WHEN c.sowedAt IS NULL THEN c.replantedAt ELSE c.sowedAt END', 'ASC')
            ->addOrderBy('c.title', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function countCurrentCultures(): int
    {
        return $this
            ->createQueryBuilder('c')
            ->select('COUNT(c.id)')
            ->where('extract(year from c.sowedAt) = :year')
            ->orWhere('extract(year from c.replantedAt) = :year')
            ->setParameter('year', (new \DateTime())->format('Y'))
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }
}
