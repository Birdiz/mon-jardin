<?php

namespace App\Controller;

use App\Repository\CultureRepository;
use App\Repository\EmplacementRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(
        EmplacementRepository $emplacementRepository,
        CultureRepository $cultureRepository,
    ): Response
    {
        return $this->render('home/index.html.twig', [
            'total_size' => $emplacementRepository->sumEmplacementSizes(),
            'total_emplacements' => $emplacementRepository->count(),
            'total_cultures' => $cultureRepository->countCurrentCultures(),
            'coming_to_sow' => $cultureRepository->getNextComingCultureToSow(),
            'coming_to_replant' => $cultureRepository->getNextComingCultureToReplant(),
            'coming_to_harvest' => $cultureRepository->getNextComingCultureToHarvest(),
        ]);
    }
}
