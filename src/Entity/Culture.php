<?php

namespace App\Entity;

use App\Repository\CultureRepository;
use App\Validator\Culture as CultureAssert;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CultureRepository::class)]
#[CultureAssert\UniqueYearlyCulture]
#[CultureAssert\RequiredDates]
class Culture
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $title = null;

    #[ORM\ManyToOne(inversedBy: 'cultures')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Emplacement $emplacement = null;

    #[ORM\ManyToOne(inversedBy: 'cultures')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Plant $plant = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $sowedAt = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $harvestedAt = null;

    #[ORM\Column(nullable: true)]
    private ?bool $isPermanant = null;

    #[ORM\Column(length: 255)]
    private ?string $quantity = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $replantedAt = null;

    public function __construct()
    {
        $this->sowedAt = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setIdNull(): static
    {
        $this->id = null;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;

        return $this;
    }

    public function getEmplacement(): ?Emplacement
    {
        return $this->emplacement;
    }

    public function setEmplacement(?Emplacement $emplacement): static
    {
        $this->emplacement = $emplacement;

        return $this;
    }

    public function getPlant(): ?Plant
    {
        return $this->plant;
    }

    public function setPlant(?Plant $plant): static
    {
        $this->plant = $plant;

        return $this;
    }

    public function getSowedAt(): ?\DateTimeImmutable
    {
        return $this->sowedAt;
    }

    public function setSowedAt(?\DateTimeImmutable $sowedAt): static
    {
        $this->sowedAt = $sowedAt;

        return $this;
    }

    public function getHarvestedAt(): ?\DateTimeImmutable
    {
        return $this->harvestedAt;
    }

    public function setHarvestedAt(?\DateTimeImmutable $harvestedAt): static
    {
        $this->harvestedAt = $harvestedAt;

        return $this;
    }

    public function isPermanant(): ?bool
    {
        return $this->isPermanant;
    }

    public function setIsPermanant(?bool $isPermanant): static
    {
        $this->isPermanant = $isPermanant;

        return $this;
    }

    public function getQuantity(): ?string
    {
        return $this->quantity;
    }

    public function setQuantity(string $quantity): static
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getReplantedAt(): ?\DateTimeImmutable
    {
        return $this->replantedAt;
    }

    public function setReplantedAt(?\DateTimeImmutable $replantedAt): static
    {
        $this->replantedAt = $replantedAt;

        return $this;
    }
}
