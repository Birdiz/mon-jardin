<?php

namespace App\Entity;

use App\Enum\CultureTypeEnum;
use App\Repository\PlantRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: PlantRepository::class)]
#[ORM\Table('plante')]
#[ORM\UniqueConstraint(columns: ['name', 'variety'])]
class Plant
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $variety = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $info = null;

    /**
     * @var Collection<int, Culture>
     */
    #[ORM\OneToMany(targetEntity: Culture::class, mappedBy: 'plant', orphanRemoval: true)]
    private Collection $cultures;

    #[ORM\Column(enumType: CultureTypeEnum::class)]
    private ?CultureTypeEnum $cultureType = null;

    public function __construct()
    {
        $this->cultures = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getVariety(): ?string
    {
        return $this->variety;
    }

    public function setVariety(?string $variety): static
    {
        $this->variety = $variety;

        return $this;
    }

    public function getInfo(): ?string
    {
        return $this->info;
    }

    public function setInfo(string $info): static
    {
        $this->info = $info;

        return $this;
    }

    /**
     * @return Collection<int, Culture>
     */
    public function getCultures(): Collection
    {
        return $this->cultures;
    }

    public function addCulture(Culture $culture): static
    {
        if (!$this->cultures->contains($culture)) {
            $this->cultures->add($culture);
            $culture->setPlant($this);
        }

        return $this;
    }

    public function removeCulture(Culture $culture): static
    {
        if ($this->cultures->removeElement($culture)) {
            // set the owning side to null (unless already changed)
            if ($culture->getPlant() === $this) {
                $culture->setPlant(null);
            }
        }

        return $this;
    }

    public function getLabel(): string
    {
        return $this->name . ' ' . $this->variety;
    }

    public function getCultureType(): ?CultureTypeEnum
    {
        return $this->cultureType;
    }

    public function setCultureType(CultureTypeEnum $cultureType): static
    {
        $this->cultureType = $cultureType;

        return $this;
    }
}
