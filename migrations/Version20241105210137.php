<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20241105210137 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Make culture harvestedAt nullable.';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE culture ALTER harvested_at DROP NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE culture ALTER harvested_at SET NOT NULL');
    }
}
