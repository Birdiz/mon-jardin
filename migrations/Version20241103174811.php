<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20241103174811 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Start entities.';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE emplacement (id SERIAL NOT NULL, title VARCHAR(255) NOT NULL, description TEXT DEFAULT NULL, size DOUBLE PRECISION NOT NULL, orientation VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE plant (id SERIAL NOT NULL, name VARCHAR(255) NOT NULL, variety VARCHAR(255) DEFAULT NULL, info TEXT NOT NULL, PRIMARY KEY(id))');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE emplacement');
        $this->addSql('DROP TABLE plant');
    }
}
