<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Doctrine\Migrations\Exception\IrreversibleMigration;

final class Version20250119174505 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add unique constraint to plant on name.';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE UNIQUE INDEX UNIQ_517A69475E237E06 ON plant (name, variety)');
    }

    public function down(Schema $schema): void
    {
        throw new IrreversibleMigration();
    }
}
