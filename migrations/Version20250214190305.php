<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Doctrine\Migrations\Exception\IrreversibleMigration;

final class Version20250214190305 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Rename plante to plant.';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE culture DROP CONSTRAINT fk_b6a99ceb177b16e8');
        $this->addSql('DROP INDEX idx_b6a99ceb177b16e8');
        $this->addSql('ALTER TABLE culture RENAME COLUMN plante_id TO plant_id');
        $this->addSql('ALTER TABLE culture ADD CONSTRAINT FK_B6A99CEB1D935652 FOREIGN KEY (plant_id) REFERENCES plante (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_B6A99CEB1D935652 ON culture (plant_id)');
        $this->addSql('ALTER INDEX uniq_517a69475e237e06 RENAME TO UNIQ_517A69475E237E0638D69117');
    }

    public function down(Schema $schema): void
    {
        throw new IrreversibleMigration();
    }
}
