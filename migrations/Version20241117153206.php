<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20241117153206 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add replantedAt date for culture.';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE culture ADD replanted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN culture.replanted_at IS \'(DC2Type:datetime_immutable)\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE culture DROP replanted_at');
    }
}
