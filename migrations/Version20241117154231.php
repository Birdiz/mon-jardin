<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20241117154231 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add cultureType to plant.';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE plant ADD culture_type VARCHAR(255) NULL');

        $this->addCultureType();

        $this->addSql('ALTER TABLE plant ALTER culture_type SET NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE plant DROP culture_type');
    }

    private function addCultureType(): void
    {
        $garlic = $this->connection->executeQuery("select id from plant where name = 'Ail blanc'");
        $this->addSql("UPDATE plant set culture_type ='repiquage'");
        $onion = $this->connection->executeQuery("select id from plant where name = 'Oignon Rocambole'");
        $this->addSql("UPDATE plant set culture_type ='repiquage'");
    }
}
