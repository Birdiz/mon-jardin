<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Doctrine\Migrations\Exception\IrreversibleMigration;

final class Version20250118091731 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Make sowedAt date for Culture nullable.';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE culture ALTER sowed_at DROP NOT NULL');
    }

    public function down(Schema $schema): void
    {
        throw new IrreversibleMigration();
    }
}
