<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20241105205836 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Make plant info nullable.';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE plant ALTER info DROP NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE plant ALTER info SET NOT NULL');
    }
}
