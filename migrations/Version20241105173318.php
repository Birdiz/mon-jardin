<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20241105173318 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add culture.';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE culture (id SERIAL NOT NULL, emplacement_id INT NOT NULL, plante_id INT NOT NULL, title VARCHAR(255) NOT NULL, sowed_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, harvested_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, is_permanant BOOLEAN DEFAULT NULL, quantity VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B6A99CEBC4598A51 ON culture (emplacement_id)');
        $this->addSql('CREATE INDEX IDX_B6A99CEB177B16E8 ON culture (plante_id)');
        $this->addSql('COMMENT ON COLUMN culture.sowed_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN culture.harvested_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE culture ADD CONSTRAINT FK_B6A99CEBC4598A51 FOREIGN KEY (emplacement_id) REFERENCES emplacement (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE culture ADD CONSTRAINT FK_B6A99CEB177B16E8 FOREIGN KEY (plante_id) REFERENCES plant (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE culture DROP CONSTRAINT FK_B6A99CEBC4598A51');
        $this->addSql('ALTER TABLE culture DROP CONSTRAINT FK_B6A99CEB177B16E8');
        $this->addSql('DROP TABLE culture');
    }
}
