--
-- PostgreSQL database dump
--

-- Dumped from database version 16.4
-- Dumped by pg_dump version 16.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: culture; Type: TABLE; Schema: public; Owner: app
--

CREATE TABLE public.culture (
    id integer NOT NULL,
    emplacement_id integer NOT NULL,
    plant_id integer NOT NULL,
    title character varying(255) NOT NULL,
    sowed_at timestamp(0) without time zone,
    harvested_at timestamp(0) without time zone,
    is_permanant boolean,
    quantity character varying(255) NOT NULL,
    replanted_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone
);


ALTER TABLE public.culture OWNER TO app;

--
-- Name: COLUMN culture.sowed_at; Type: COMMENT; Schema: public; Owner: app
--

COMMENT ON COLUMN public.culture.sowed_at IS '(DC2Type:datetime_immutable)';


--
-- Name: COLUMN culture.harvested_at; Type: COMMENT; Schema: public; Owner: app
--

COMMENT ON COLUMN public.culture.harvested_at IS '(DC2Type:datetime_immutable)';


--
-- Name: COLUMN culture.replanted_at; Type: COMMENT; Schema: public; Owner: app
--

COMMENT ON COLUMN public.culture.replanted_at IS '(DC2Type:datetime_immutable)';


--
-- Name: culture_id_seq; Type: SEQUENCE; Schema: public; Owner: app
--

CREATE SEQUENCE public.culture_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.culture_id_seq OWNER TO app;

--
-- Name: culture_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: app
--

ALTER SEQUENCE public.culture_id_seq OWNED BY public.culture.id;


--
-- Name: doctrine_migration_versions; Type: TABLE; Schema: public; Owner: app
--

CREATE TABLE public.doctrine_migration_versions (
    version character varying(191) NOT NULL,
    executed_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    execution_time integer
);


ALTER TABLE public.doctrine_migration_versions OWNER TO app;

--
-- Name: emplacement; Type: TABLE; Schema: public; Owner: app
--

CREATE TABLE public.emplacement (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    description text,
    size double precision NOT NULL,
    orientation character varying(255) DEFAULT NULL::character varying
);


ALTER TABLE public.emplacement OWNER TO app;

--
-- Name: emplacement_id_seq; Type: SEQUENCE; Schema: public; Owner: app
--

CREATE SEQUENCE public.emplacement_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.emplacement_id_seq OWNER TO app;

--
-- Name: emplacement_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: app
--

ALTER SEQUENCE public.emplacement_id_seq OWNED BY public.emplacement.id;


--
-- Name: plante; Type: TABLE; Schema: public; Owner: app
--

CREATE TABLE public.plante (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    variety character varying(255) DEFAULT NULL::character varying,
    info text,
    culture_type character varying(255) NOT NULL
);


ALTER TABLE public.plante OWNER TO app;

--
-- Name: plante_id_seq; Type: SEQUENCE; Schema: public; Owner: app
--

CREATE SEQUENCE public.plante_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.plante_id_seq OWNER TO app;

--
-- Name: plante_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: app
--

ALTER SEQUENCE public.plante_id_seq OWNED BY public.plante.id;


--
-- Name: culture id; Type: DEFAULT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.culture ALTER COLUMN id SET DEFAULT nextval('public.culture_id_seq'::regclass);


--
-- Name: emplacement id; Type: DEFAULT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.emplacement ALTER COLUMN id SET DEFAULT nextval('public.emplacement_id_seq'::regclass);


--
-- Name: plante id; Type: DEFAULT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.plante ALTER COLUMN id SET DEFAULT nextval('public.plante_id_seq'::regclass);


--
-- Data for Name: culture; Type: TABLE DATA; Schema: public; Owner: app
--

COPY public.culture (id, emplacement_id, plant_id, title, sowed_at, harvested_at, is_permanant, quantity, replanted_at) FROM stdin;
1	1	1	Ail blanc	2024-11-03 13:00:00	2025-06-10 13:00:00	f	1 ligne	\N
4	12	1	Ail blanc	2024-11-03 00:00:00	2025-06-10 00:00:00	f	1 ligne	\N
5	1	23	Pois	2025-03-05 00:00:00	\N	f	Butte complète	\N
6	16	5	Haricots de Soissons	2025-02-20 00:00:00	\N	f	Quatre tuteurs	\N
7	20	5	Haricots de Soissons	2025-02-20 00:00:00	\N	f	Quatre tuteurs	\N
8	11	37	Oignon bulbes	\N	2025-08-01 00:00:00	f	2 lignes	2025-03-20 00:00:00
9	3	18	Épinard	2025-03-15 00:00:00	\N	f	2 lignes	\N
12	11	18	Épinard	2025-03-15 00:00:00	\N	f	2 lignes	\N
13	3	35	Laitue	2025-03-30 00:00:00	\N	f	1 ligne	\N
15	11	35	Laitue	2025-03-30 00:00:00	\N	f	1 ligne	\N
14	14	35	Laitue	2025-04-15 00:00:00	\N	f	2 lignes	\N
11	14	18	Épinard	2025-03-30 00:00:00	\N	f	2 lignes	\N
16	14	2	Oignon Rocambole	2024-05-05 00:00:00	\N	t	5 plants	\N
17	11	38	Roquette	2025-03-30 00:00:00	\N	f	2 lignes	\N
18	10	38	Roquette	2025-03-30 00:00:00	\N	f	2 lignes	\N
19	15	5	Haricots de Soissons	2025-03-10 00:00:00	\N	f	Quatre tuteurs	\N
20	15	8	Pommes de terre	2025-04-15 00:00:00	\N	f	Butte complète	\N
21	4	33	Poireaux d'été	2025-03-20 00:00:00	\N	f	pépinière sous abri	2025-05-01 00:00:00
22	6	33	Poireaux	2025-06-01 00:00:00	\N	f	Pépinière	2025-09-01 00:00:00
23	9	31	Choux de Bruxelles	2025-03-20 00:00:00	\N	f	pépinière sous abri	2025-05-01 00:00:00
24	5	31	Choux-fleurs et Choux de Milan	2025-03-20 00:00:00	\N	f	Pépinière sous abri	2025-05-01 00:00:00
25	8	3	Carottes	2025-04-01 00:00:00	\N	f	Bac complet	\N
26	21	3	Carottes	2025-04-01 00:00:00	\N	f	Butte complète	\N
27	18	11	Bette	2025-04-13 00:00:00	\N	f	1 ligne	\N
28	18	32	Concombre	2025-04-25 00:00:00	\N	f	8 godets	2025-06-01 00:00:00
29	22	17	Potimarron	2025-04-25 00:00:00	\N	f	5 plants	\N
32	16	4	Échalotte	2025-05-15 00:00:00	2025-07-20 00:00:00	f	2 lignes	\N
33	6	4	Échalotte	2025-05-15 00:00:00	2025-07-20 00:00:00	f	2 lignes	\N
34	17	35	Endive	2025-05-20 00:00:00	\N	f	2 lignes	2025-10-10 00:00:00
35	17	20	Mâche	2025-07-15 00:00:00	\N	f	1 ligne	\N
36	6	20	Mâche	2025-08-15 00:00:00	\N	f	2 lignes	\N
37	19	39	Maïs	2025-06-01 00:00:00	2025-09-15 00:00:00	f	2 lignes	\N
31	11	30	Courgette	2025-04-15 00:00:00	\N	f	2 plants	2025-06-01 00:00:00
30	16	30	Courgette	2025-04-01 00:00:00	\N	f	2 plants	2025-05-15 00:00:00
\.


--
-- Data for Name: doctrine_migration_versions; Type: TABLE DATA; Schema: public; Owner: app
--

COPY public.doctrine_migration_versions (version, executed_at, execution_time) FROM stdin;
DoctrineMigrations\\Version20241103174811	2024-11-04 17:03:08	18
DoctrineMigrations\\Version20241105173318	2024-11-05 17:33:53	32
DoctrineMigrations\\Version20241105205836	2024-11-05 20:58:54	9
DoctrineMigrations\\Version20241105210137	2024-11-05 21:02:18	6
DoctrineMigrations\\Version20241117153206	2024-11-17 15:32:23	15
DoctrineMigrations\\Version20241117154231	2024-11-17 15:49:18	10
DoctrineMigrations\\Version20250118091731	2025-01-18 09:18:22	7
DoctrineMigrations\\Version20250119174505	2025-01-19 17:46:51	8
DoctrineMigrations\\Version20250214190305	2025-02-14 19:04:09	26
\.


--
-- Data for Name: emplacement; Type: TABLE DATA; Schema: public; Owner: app
--

COPY public.emplacement (id, title, description, size, orientation) FROM stdin;
1	Station météo 1	Là où la station météo est positionnée, sur le côté de l'olivier.	2.5	N/S
2	Station météo 2	Là où la station météo est positionnée, sur le côté du chêne.	2.5	N/S
3	Les tombes 1	Première petite butte dans la montée.	0.5	E/O
4	Les tombes 2	Deuxième petite butte dans la montée.	0.9	E/O
5	Les tombes 3	Troisième petite butte dans la montée.	1	E/O
6	Les tombes 4	Quatrième petite butte dans la montée.	1.75	E/O
8	Bac cour 1	Vers la rue.	1.5	E/O
9	Bac cour 2	Vers le terrain.	1.5	E/O
10	Bac-serre	Bac palette avec vitre tracteur.	1	N/S
13	Butte 3	Butte derrière la butte 2.	2	NE/SO
12	Butte 1	Butte derrière l'olivier orientée vers la mare.	2.5	N/S
11	Butte coffrée 1	Butte avec cadre bois relativement plus haute que les autres.	2.1	N/S
17	Butte 2	Butte derrière la butte coffrée 1, perpendiculaire de la butte 1.	2	NE/SO
14	Butte coffrée 2	Butte entre la butte 1 et la mare.	3.3	N/S
15	Butte coffrée 3	Entre la butte coffrée 2 et la petite mare (vers le gîte).	3.3	N/S
16	Butte 5	Butte au dessus des buttes 2 & 3, devant le bac-serre, avec le pied de sauge.	2	N/S
21	Butte 4	Butte derrière la butte 3, avant la source.	2.2	NE/SO
18	Butte 6	Première butte en montant vers la pergola.	1.7	N/S
20	Butte 8	Butte sur le chemin vers les arbres fruitiers.	1.7	N/S
19	Butte 7	Butte au dessus de la butte 6.	1.7	N/S
22	Compost	N'importe quel tas de composte demi-mûr	1	N/S
\.


--
-- Data for Name: plante; Type: TABLE DATA; Schema: public; Owner: app
--

COPY public.plante (id, name, variety, info, culture_type) FROM stdin;
2	Oignon	rocambole	(Bulbilles)	direct
6	Mesclun	\N	\N	direct
7	Panais	\N	\N	direct
10	Topinambour	\N	\N	direct
11	Bette	\N	\N	direct ou repiquage
12	Betterave	\N	\N	direct ou repiquage
13	Céléri-rave	\N	\N	direct ou repiquage
14	Chou	kale	\N	direct ou repiquage
15	Chou-rave	\N	\N	direct ou repiquage
16	Chou	chinois	\N	direct ou repiquage
26	Tétragone	\N	Épinard d'été.	direct ou repiquage
22	Mizuna	\N	Sorte de chou/salade (façon roquette).	direct ou repiquage
27	Artichaut	\N	\N	repiquage
28	Aubergine	\N	\N	repiquage
29	Cornichon	\N	\N	repiquage
31	Chou	\N	\N	repiquage
32	Concombre	\N	\N	repiquage
34	Poivron	\N	\N	repiquage
35	Salade	\N	\N	direct ou repiquage
36	Tomate	\N	\N	repiquage
38	Roquette	\N	\N	direct
39	Maïs	\N	\N	direct ou repiquage
8	Pomme de terre	\N	(Plants). Désherber, sillons de 5 cm de profond, paillage 20cm. Amender en composte demi-mûr + potasse (cendre de bois).	direct
3	Carotte	\N	Pas d'apport.	direct
24	Potiron	\N	Compost demi-mûr.	direct ou repiquage
17	Courge	\N	Compost demi-mûr.	direct ou repiquage
30	Courgette	\N	Compost demi-mûr.	repiquage
4	Echalotte	\N	(Bulbilles). Pas d'apport.	direct
1	Ail	blanc	(Caïeux) ; Dans un sol léger, sans apport, idéal butté.	direct
18	Épinard	\N	Compost bien mûr.	direct ou repiquage
5	Fève	\N	Pas d'apport.	direct
19	Haricot	\N	Pas d'apport.	direct ou repiquage
20	Mâche	\N	Pas d'apport.	direct ou repiquage
25	Navet	\N	Pas d'apport.	direct ou repiquage
37	Oignon bulbe	\N	Bien enfoncer en terre. Pas d'apport.	repiquage
33	Poireau	\N	Butter.	repiquage
23	Pois	\N	Pas d'apport.	direct ou repiquage
9	Radis	\N	Pas d'apport.	direct
\.


--
-- Name: culture_id_seq; Type: SEQUENCE SET; Schema: public; Owner: app
--

SELECT pg_catalog.setval('public.culture_id_seq', 37, true);


--
-- Name: emplacement_id_seq; Type: SEQUENCE SET; Schema: public; Owner: app
--

SELECT pg_catalog.setval('public.emplacement_id_seq', 22, true);


--
-- Name: plante_id_seq; Type: SEQUENCE SET; Schema: public; Owner: app
--

SELECT pg_catalog.setval('public.plante_id_seq', 39, true);


--
-- Name: culture culture_pkey; Type: CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.culture
    ADD CONSTRAINT culture_pkey PRIMARY KEY (id);


--
-- Name: doctrine_migration_versions doctrine_migration_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.doctrine_migration_versions
    ADD CONSTRAINT doctrine_migration_versions_pkey PRIMARY KEY (version);


--
-- Name: emplacement emplacement_pkey; Type: CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.emplacement
    ADD CONSTRAINT emplacement_pkey PRIMARY KEY (id);


--
-- Name: plante plante_pkey; Type: CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.plante
    ADD CONSTRAINT plante_pkey PRIMARY KEY (id);


--
-- Name: idx_b6a99ceb1d935652; Type: INDEX; Schema: public; Owner: app
--

CREATE INDEX idx_b6a99ceb1d935652 ON public.culture USING btree (plant_id);


--
-- Name: idx_b6a99cebc4598a51; Type: INDEX; Schema: public; Owner: app
--

CREATE INDEX idx_b6a99cebc4598a51 ON public.culture USING btree (emplacement_id);


--
-- Name: uniq_517a69475e237e0638d69117; Type: INDEX; Schema: public; Owner: app
--

CREATE UNIQUE INDEX uniq_517a69475e237e0638d69117 ON public.plante USING btree (name, variety);


--
-- Name: culture fk_b6a99ceb1d935652; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.culture
    ADD CONSTRAINT fk_b6a99ceb1d935652 FOREIGN KEY (plant_id) REFERENCES public.plante(id);


--
-- Name: culture fk_b6a99cebc4598a51; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.culture
    ADD CONSTRAINT fk_b6a99cebc4598a51 FOREIGN KEY (emplacement_id) REFERENCES public.emplacement(id);


--
-- PostgreSQL database dump complete
--

